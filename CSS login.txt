
# Login
.Rounded-Rectangle-4 {
  width: 438px;
  height: 50px;
  border-radius: 25px;
  box-shadow: 0px 4px 32px 0 rgba(45, 60, 240, 0.11);
  border: solid 2px #149de4;
  background-image: linear-gradient(to bottom, #081b4a, #102967);
}

# texte login
.Amineramigmailcom {
  width: 216px;
  height: 21px;
  font-family: Nexa;
  font-size: 20px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
}

# MDP
.Rounded-Rectangle-4-copy {
  width: 438px;
  height: 50px;
  border-radius: 25px;
  box-shadow: 0px 4px 32px 0 rgba(45, 60, 240, 0.11);
  border: solid 2px #149de4;
  background-image: linear-gradient(to bottom, #081b4a, #102967);
}

# Connexion
.Rounded-Rectangle-5 {
  width: 246px;
  height: 44px;
  border-radius: 22px;
  box-shadow: 0px 4px 32px 0 rgba(45, 60, 240, 0.26);
  background-image: linear-gradient(to bottom, #3a09f3, #139aeb);
}

#Test Connexion
.CONNEXION {
  width: 105px;
  height: 14px;
  font-family: Nexa;
  font-size: 18px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
}

# Bonde Discover your Data
.Rounded-Rectangle-3 {
  width: 130px;
  height: 4px;
  border-radius: 2px;
  background-image: linear-gradient(267deg, #14e087, #6ec1f3);
}

